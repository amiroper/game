### How to run service

In order to run the application first checkout the code and run the following:

1. `docker-compose build`

2. `docker-compose up`

3.  [View the Swagger API documentation localhost:8080](http://localhost:8080/)

### Notes

- This system can host multiple applications.
- After creation of each application you have to use the ID of the application (app_id) for performing operations
around that application.
- The default swagger documentation for endpoint to turn game On/Off is not correct
#### Content negotiation [ JSON / LD+JSON ]
All of the endpoints will return `json` data format by default, in the example outputs I noticed all contents are inside
 `data`, if the expected format is to be identical to example, we need to add a serializer to place all content inside a
 `data` field.

 If that is not a hard requirement and we just need to see all content in a data field so we could add more metadata
later, it would be better to use `ld+json` during content negotiation and receive data like this.
```
-H accept: application/json
vs
-H accept: application/ld+json
```
Example `ld+json` output
```
{
  "@context": "/contexts/Application",
  "@id": "/applications",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "label": "My Profile",
      "key": "my_profile",
      "games": [
        {
          "@id": "/games/55",
          "@type": "Game",
          "label": "Explore App",
          "key": "explore_app"
        }
      ]
    },
    {
      "label": "My Actions",
      "key": "actions",
      "games": [
        {
          "@id": "/games/54",
          "@type": "Game",
          "label": "Attendee Networking",
          "key": "attendee_networking"
        },
        {
          "@id": "/games/55",
          "@type": "Game",
          "label": "Explore App",
          "key": "explore_app"
        }
      ]
    },
    {
      "label": "My Network",
      "key": "my_network",
      "games": [
        {
          "@id": "/games/54",
          "@type": "Game",
          "label": "Attendee Networking",
          "key": "attendee_networking"
        }
      ]
    }
  ],
  "hydra:totalItems": 3
}

```
### Endpoints
#### Endpoint to create an application
##### URL
```
POST /applications
```
##### Request example
```
{
   "sections":[
      {
         "key":"my_profile",
         "label":"My Profile"
      },
      {
         "key":"actions",
         "label":"My Actions"
      },
      {
         "key":"my_network",
         "label":"My Network"
      }
   ],
   "games":[
      {
         "key":"attendee_networking",
         "label":"Attendee Networking",
         "sections":[
            "my_network",
            "actions"
         ]
      },
      {
         "key":"explore_app",
         "label":"Explore App",
         "sections":[
            "my_profile",
            "actions"
         ]
      }
   ]
}
```
##### Response example
```
{
  "app_id": 25
}
```
```app_id``` is the ID of the created application


#### Endpoint to get list of available games in an app
##### URL
```
GET /applications/{app_id}/games
```
```app_id``` is the ID of the application

##### Response example
```
[
  {
    "label": "Attendee Networking",
    "key": "attendee_networking"
  },
  {
    "label": "Explore App",
    "key": "explore_app"
  }
]
```

#### Endpoint to get enabled games in their sections
##### URL
```
GET /applications/{app_id}
```
```app_id``` is the ID of the application

##### Response example
```
[
  {
    "label": "My Profile",
    "key": "my_profile",
    "games": [
      {
        "label": "Explore App",
        "key": "explore_app"
      }
    ]
  },
  {
    "label": "My Actions",
    "key": "actions",
    "games": [
      {
        "label": "Attendee Networking",
        "key": "attendee_networking"
      },
      {
        "label": "Explore App",
        "key": "explore_app"
      }
    ]
  },
  {
    "label": "My Network",
    "key": "my_network",
    "games": [
      {
        "label": "Attendee Networking",
        "key": "attendee_networking"
      }
    ]
  }
]
```

#### Endpoint to turn game On/Off
- NOTE: The default swagger documentation for this endpoint is not correct

##### URL
```
PATCH /applications/{app_id}
```
```app_id``` is the ID of the application

##### Request example
```
{
  "attendee_networking": 1,
  "explore_app":0
}
```

##### Response example
```
{
  "games_affected": 2
}
```
```games_affected``` is the number of games where their status changed

