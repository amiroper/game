<?php

namespace App\Controller;

use App\Entity\Section;
use App\Repository\SectionRepository;
use Doctrine\ORM\EntityManagerInterface;

class ActiveGamesOperation
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em) {

        $this->em = $em;
    }

    public function __invoke($data)
    {
        /** @var SectionRepository $repo */
        $repo = $this->em->getRepository(Section::class);
        return $repo->findAllSectionsWithActiveGamesForApplication($data);
    }
}
