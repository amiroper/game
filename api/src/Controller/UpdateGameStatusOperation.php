<?php

namespace App\Controller;

use App\Entity\Game;
use App\Repository\ApplicationRepository;
use App\Repository\GameRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UpdateGameStatusOperation
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em) {

        $this->em = $em;
    }

    public function __invoke(Request $data, ApplicationRepository $applicationRepository, GameRepository $gameRepository)
    {
        $applicationId = (int) $data->attributes->get('id');
        $application = $applicationRepository->find($applicationId);
        if (!$application) {
            throw new NotFoundHttpException();
        }

        $contentString = $data->getContent();
        try {
            $contentJson = json_decode($contentString, true, 2 , JSON_THROW_ON_ERROR);
        } /** @noinspection PhpUndefinedClassInspection */
        catch (\JsonException $e) {
            throw new BadRequestHttpException('Invalid json format');
        }
        foreach ($contentJson as $record) {
            if ($record !== 0 && $record !== 1) {
                throw new BadRequestHttpException('Game status should be 0 or 1');
            }
        }
        /** @var Game[] $games */
        $games = $gameRepository->findByKey(array_keys($contentJson));
        // We could check here and make sure we found all requested games or throw an exception
        $updatedGames = 0;
        foreach ($games as $game) {
            $newEnabledStatus = $contentJson[$game->getKey()]===1;
            if ($game->getEnabled() !== $newEnabledStatus) {
                $game->setEnabled($contentJson[$game->getKey()]===1);
                $this->em->persist($game);
                $updatedGames++;
            }

        }
        $this->em->flush();
        return ['games_affected' => $updatedGames];
    }
}
