<?php

namespace App\Controller;

use App\Entity\Application;
use App\Entity\Game;
use App\Entity\Section;
use Doctrine\ORM\EntityManagerInterface;

class CreateApplicationOperation
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em) {

        $this->em = $em;
    }

    public function __invoke(Application $data)
    {
        $application = new Application();
        $this->em->persist($application);

        $sectionLookup = [];
        foreach ($data->getSections() as $section) {
            $newSection = new Section();
            $newSection->setLabel($section->getLabel())->setKey($section->getKey())->setApplication($application);
            $sectionLookup[$section->getKey()] = $newSection;
            $this->em->persist($newSection);
        }
        foreach ($data->getGames() as $game) {
            $newGame = new Game();
            $newGame->setKey($game->getKey())->setLabel($game->getLabel())->setEnabled(true)->setApplication($application);
            foreach ($game->getSections() as $gameSection) {
                   $newGame->addAssignedSection($sectionLookup[$gameSection]);
            }
            $this->em->persist($newGame);
        }
        $this->em->flush();
        return ['app_id' => $application->getId()];
    }
}
