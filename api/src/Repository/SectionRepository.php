<?php

namespace App\Repository;

use App\Entity\Application;
use App\Entity\Section;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Section|null find($id, $lockMode = null, $lockVersion = null)
 * @method Section|null findOneBy(array $criteria, array $orderBy = null)
 * @method Section[]    findAll()
 * @method Section[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Section::class);
    }

    public function findAllSectionsWithActiveGamesForApplication(Application $application)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.application = :app_id')
            ->setParameter('app_id', $application->getId())
            ->innerJoin('s.games', 'g')
            ->andWhere('g.enabled = 1')
            ->addSelect('g')
            ->getQuery()
            ->getResult();
    }
}
