<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Action\NotFoundAction;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"write"}},
 *     normalizationContext={"groups"={"read"}},
 *     collectionOperations={},
 *     itemOperations={
 *         "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *
 *         },
 *      },
 *    )
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 */
class Game
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"write","read"})
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application", inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $application;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled = true;

    /**
     * @ORM\Column(type="string", length=255, name="gameKey")
     * @Groups({"write","read"})
     */
    private $key;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Section", inversedBy="games")
     */
    private $assignedSections;

    /**
     * @Groups({"write"})
     */
    private $sections;

    public function __construct()
    {
        $this->assignedSections = new ArrayCollection();
        $this->sections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getApplication(): ?Application
    {
        return $this->application;
    }

    public function setApplication(?Application $application): self
    {
        $this->application = $application;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return Collection|Section[]
     */
    public function getAssignedSections(): Collection
    {
        return $this->assignedSections;
    }

    public function addAssignedSection(Section $section): self
    {
        if (!$this->assignedSections->contains($section)) {
            $this->assignedSections[] = $section;
        }

        return $this;
    }

    public function removeAssignedSection(Section $section): self
    {
        if ($this->assignedSections->contains($section)) {
            $this->assignedSections->removeElement($section);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * @param mixed $sections
     */
    public function setSections($sections): void
    {
        $this->sections = $sections;
    }
    public function addSection($section): self
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
        }

        return $this;
    }

    public function removeSection(Section $section): self
    {
        if ($this->sections->contains($section)) {
            $this->sections->removeElement($section);
        }

        return $this;
    }
}
