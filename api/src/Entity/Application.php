<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\CreateApplicationOperation;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\ActiveGamesOperation;
use App\Controller\UpdateGameStatusOperation;
/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"write"}},
 *     normalizationContext={"groups"={"read"}},
 *     itemOperations={
 *         "get"={
 *             "controller"=ActiveGamesOperation::class,
 *         },
 *         "patch"={
 *             "read" = false,
 *             "controller"=UpdateGameStatusOperation::class,
 *             "deserialize"=false,
 *         },
 *      },
 *     collectionOperations={
 *     "get",
 *     "post"={
 *         "method"="POST",
 *         "controller"=CreateApplicationOperation::class
 *     },
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\ApplicationRepository")
 */
class Application
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdTime;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Section", mappedBy="application", orphanRemoval=true)
     * @Groups({"write", "read"})
     */
    private $sections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Game", mappedBy="application", orphanRemoval=true)
     * @ApiSubresource()
     * @Groups({"write", "read"})
     */
    private $games;


    public function __construct()
    {
        $this->sections = new ArrayCollection();
        $this->games = new ArrayCollection();
        $this->createdTime = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedTime(): ?\DateTimeInterface
    {
        return $this->createdTime;
    }

    /**
     * @return Collection|Section[]
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    public function addSection(Section $section): self
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
            $section->setApplication($this);
        }

        return $this;
    }

    public function removeSection(Section $section): self
    {
        if ($this->sections->contains($section)) {
            $this->sections->removeElement($section);
            // set the owning side to null (unless already changed)
            if ($section->getApplication() === $this) {
                $section->setApplication(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->setApplication($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->contains($game)) {
            $this->games->removeElement($game);
            // set the owning side to null (unless already changed)
            if ($game->getApplication() === $this) {
                $game->setApplication(null);
            }
        }

        return $this;
    }

}
