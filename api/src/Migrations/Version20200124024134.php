<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200124024134 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE application (id INT AUTO_INCREMENT NOT NULL, created_time DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE section (id INT AUTO_INCREMENT NOT NULL, application_id INT NOT NULL, label VARCHAR(255) NOT NULL, section_key VARCHAR(255) NOT NULL, INDEX IDX_2D737AEF3E030ACD (application_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, application_id INT NOT NULL, label VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, game_key VARCHAR(255) NOT NULL, INDEX IDX_232B318C3E030ACD (application_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_section (game_id INT NOT NULL, section_id INT NOT NULL, INDEX IDX_B8B105C0E48FD905 (game_id), INDEX IDX_B8B105C0D823E37A (section_id), PRIMARY KEY(game_id, section_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE section ADD CONSTRAINT FK_2D737AEF3E030ACD FOREIGN KEY (application_id) REFERENCES application (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C3E030ACD FOREIGN KEY (application_id) REFERENCES application (id)');
        $this->addSql('ALTER TABLE game_section ADD CONSTRAINT FK_B8B105C0E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_section ADD CONSTRAINT FK_B8B105C0D823E37A FOREIGN KEY (section_id) REFERENCES section (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEF3E030ACD');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C3E030ACD');
        $this->addSql('ALTER TABLE game_section DROP FOREIGN KEY FK_B8B105C0D823E37A');
        $this->addSql('ALTER TABLE game_section DROP FOREIGN KEY FK_B8B105C0E48FD905');
        $this->addSql('DROP TABLE application');
        $this->addSql('DROP TABLE section');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE game_section');
    }
}
